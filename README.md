# Custom Google Map


This project focuses on a custom Google Map widget that can be easily utilized throughout the application, eliminating the need for repetitive code. This custom widget provides a seamless integration of Google Maps functionality and can be used efficiently across different parts of the application.

## Getting Started

In this project, I have implemented features such as 
- creating polygons
- obtaining the device's current location
- creating polylines
- searching for addresses, and retrieving their corresponding locations
- Map Camera Moment
- Full and half screen functionality 
- Visibility for My Current location button
- Visibility for Zoom Controls button




For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
