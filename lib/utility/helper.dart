import 'dart:convert';
import 'package:android_intent_plus/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_map/main.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;
import 'package:another_flushbar/flushbar.dart';

import '../model/model_address.dart';

class HelperFunction {
  //This method is used sow success messages
  static showFlushBarSuccess(BuildContext context, String msg) {
    return Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      backgroundColor: Colors.green,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: const Duration(milliseconds: 900),
    )..show(context);
  }

  //This method is used sow failure messages
  static showFlushBarError(BuildContext context, String? msg) {
    return Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      backgroundColor: Colors.red,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: const Duration(seconds: 2),
    )..show(context);
  }

// This is alert box use show your gps is off, using this we can turn on the gps
  static gpsAlert(BuildContext context) async {
    return await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Can't get Current location"),
          content: const Text('Please make sure you enable GPS and try again'),
          actions: <Widget>[
            TextButton(
              child: Text('Turn On'),
              onPressed: () {
                final AndroidIntent intent = AndroidIntent(
                    action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                intent.launch();
                Navigator.of(context, rootNavigator: true).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

// To detect GPS status
  static isGps() async {
    return await Geolocator.isLocationServiceEnabled();
  }

// This method is use te get current location of the device
  static Future<Position?> getCurrentLocation(
      {LocationAccuracy? desiredAccuracy, Duration? timeLimit}) async {
    try {
      bool serviceEnabled;
      LocationPermission permission;
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      return await Geolocator.getCurrentPosition(
          timeLimit: timeLimit,
          desiredAccuracy: desiredAccuracy ?? LocationAccuracy.best);
    } catch (error) {
      return null;
    }
  }

  // This method is used for draw custom marker on the map
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

// This method is used for  get accurate address from the selected location
  static Future<ModelAddress?> getPlaceAddress(double lat, double lng) async {
    final url = Uri.parse(
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$googleMapKey');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final List address = json.decode(response.body)['results'];

      String? getAddress({required String filterBy, int? index}) {
        String? addressField;

        for (var element in address[index ?? 0]['address_components']) {
          if (element['types'].contains(filterBy)) {
            addressField = element["long_name"];
            break;
          }
        }
        return addressField;
      }

      return ModelAddress(
        latitude: lat,
        longitude: lng,
        locality: getAddress(
              filterBy: "locality",
            ) ??
            "",
        pincode: getAddress(
                filterBy: "postal_code",
                index: address.indexWhere(
                    (element) => element['types'].contains('postal_code'))) ??
            "",
        address: address[0]['formatted_address'],
        country: getAddress(filterBy: "country") ?? "",
        state: getAddress(filterBy: "administrative_area_level_1") ?? "",

        district: getAddress(filterBy: "administrative_area_level_3") ?? "",
        village: getAddress(
              filterBy: "sublocality_level_1",
            ) ??
            getAddress(
              filterBy: "locality",
            ) ??
            "",
        taluka: getAddress(
                filterBy: "administrative_area_level_4",
                index: address.indexWhere((element) => element['types']
                    .contains('administrative_area_level_4'))) ??
            "", // taluka details is not present at all index need to find that index.
      );
    }
    return null; // return null if the request fails or no results are found
  }
}
