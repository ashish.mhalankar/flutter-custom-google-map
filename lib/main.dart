import 'package:flutter/material.dart';
import 'package:flutter_google_map/widgets/widget_goolge_map.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:custom_info_window/custom_info_window.dart';

String googleMapKey = "AIzaSyBUnoJi-OhbR0PgOxKTew1kWE-dslp8o0w";
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Google Map',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const MyHomePage(title: 'Custom Google Map'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  GoogleMapController? googleMapController;
  double _currentZoom = 17;
  CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();
  Set<Polygon> polygonSet = Set();
  final Set<Marker> _markers = Set();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: WidgetGoogleMap(
        markerSet: _currentZoom <= 16 ? _markers : <Marker>{},
        polygonSet: _currentZoom > 16 ? polygonSet : <Polygon>{},
        onMapCreated: (controller) {
          googleMapController = controller;
          _customInfoWindowController.googleMapController = googleMapController;
        },
        onTap: (p0, p1) {
          // _customInfoWindowController.hideInfoWindow!();
        },
        myLocationEnabled: true,
        createPolygon: (p0) {},
        searchedAddress: (p0) {},
        onCameraIdle: () {},
        onCameraMoveStarted: () {
          // _customInfoWindowController.hideInfoWindow!();
        },
      ),
    );
  }
}
