import 'dart:async';
import 'dart:collection';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_map/main.dart';
import 'package:flutter_google_map/model/model_address.dart';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:collection/collection.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';

import '../utility/helper.dart';

class WidgetGoogleMap extends StatefulWidget {
  CameraPositionCallback? onCameraMove;
  VoidCallback? onCameraIdle;
  VoidCallback? onCameraMoveStarted;
  Function(LatLng, ModelAddress)? onTap;
  MapCreatedCallback? onMapCreated;

  Set<Polygon>? polygonSet = new Set();
  Set<Polyline>? polylineSet = new Set();
  Set<Marker>? markerSet = new Set();

  Function(bool)? onFullScreenClick;
  Function(List<LatLng>)? createPolygon;
  LatLng? currentLocation;
  Function(Position?)? position;

  Function(ModelAddress)? searchedAddress;

  bool? myLocationEnabled,
      myLocationButtonEnabled,
      zoomControlsEnabled,
      hideToogleLeftColumn;

  WidgetGoogleMap({
    this.currentLocation,
    this.polygonSet,
    this.markerSet,
    this.polylineSet,
    this.onCameraIdle,
    this.onTap,
    this.onMapCreated,
    this.onCameraMove,
    this.onCameraMoveStarted,
    this.onFullScreenClick,
    this.createPolygon,
    this.position,
    this.searchedAddress,
    this.myLocationEnabled,
    this.myLocationButtonEnabled,
    this.zoomControlsEnabled,
    this.hideToogleLeftColumn,
  });
  @override
  _WidgetGoogleMapState createState() => _WidgetGoogleMapState();
}

class _WidgetGoogleMapState extends State<WidgetGoogleMap> {
  static const iconMarker = 'asset/images/markerIcon.png';

  bool isMapTypeNormal = false,
      isPolygonCreated = false,
      fullScreen = false,
      isAreaContains = false;

  Set<Marker>? markers = HashSet<Marker>();
  Set<Polyline>? polylines = HashSet<Polyline>();
  Set<Polygon>? polygons = HashSet<Polygon>();

  List<LatLng> polygonLatLongs = [];
  List<LatLng> polylineLatLongs = [];
  double mapZoomValue = 16.5;

  final Completer<GoogleMapController> _controller = Completer();

  CameraPosition? _kGooglePlex;

  LatLng? _lastMapPosition;
  GoogleMapController? controller;

  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  final searchScaffoldKey = GlobalKey<ScaffoldState>();
  // ignore: missing_required_param
  ModelAddress currentAddress = ModelAddress();

  @override
  void initState() {
    super.initState();
    didChangeDependencies();

    if (widget.polygonSet != null) {
      setState(() {
        polygons = widget.polygonSet;
      });
    }
    if (widget.polylineSet != null) {
      setState(() {
        polylines = widget.polylineSet;
      });
    }

    if (widget.markerSet != null) {
      setState(() {
        markers = widget.markerSet;
      });
    }
    if (widget.currentLocation != null) {
      setState(() {
        _kGooglePlex =
            CameraPosition(target: widget.currentLocation!, zoom: mapZoomValue);
      });
    } else {
      checkIsGpsOn();
    }
  }

  @override
  void didUpdateWidget(covariant WidgetGoogleMap oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.markerSet != null) {
      setState(() {
        markers = widget.markerSet;
      });
    }
    if (widget.polygonSet != null) {
      setState(() {
        polygons = widget.polygonSet;
      });
    }
  }

  checkIsGpsOn() async {
    bool isGpsOn = await HelperFunction.isGps();
    if (isGpsOn) {
      await HelperFunction.getCurrentLocation().then((value) {
        setState(() {
          _kGooglePlex = CameraPosition(
              target: LatLng(value!.latitude, value.longitude),
              zoom: mapZoomValue);

          _lastMapPosition = LatLng(value.latitude, value.longitude);
        });

        if (widget.position != null) {
          widget.position!(value);
        }
      });
    } else {
      HelperFunction.gpsAlert(context).then((val) {
        Future.delayed(const Duration(seconds: 3), () {
          checkIsGpsOn();
        });
      });
    }
  }

  void updateNewMarkerLocation(String id, LatLng _newLoc,
      {bool? dragEnd}) async {
    polygonLatLongs[int.parse(id) - 1] = _newLoc;
    List<LatLng> listTempPolygonLatLongs = [...polygonLatLongs];
    if (listTempPolygonLatLongs.last.latitude !=
        listTempPolygonLatLongs.first.latitude)
      listTempPolygonLatLongs.add(polygonLatLongs.first);
    polygons!.removeWhere((element) =>
        element.polygonId == PolygonId("NewPloygon${polygons!.length - 1}"));

    listTempPolygonLatLongs[int.parse(id) - 1] = _newLoc;
    setState(() {
      createPolygon(listTempPolygonLatLongs);

      if (listTempPolygonLatLongs.length >= 4) {
        List<dynamic> list = [];

        listTempPolygonLatLongs.forEach((element) {
          list.add([element.longitude, element.latitude]);
        });
      }

      widget.createPolygon!(listTempPolygonLatLongs);
    });
  }

  addMarker(
      {required LatLng latLng,
      required String id,
      required Uint8List markerIcon}) async {
    //custom marker

    markers!.add(
      Marker(
        draggable: true,
        markerId: MarkerId(id),
        position: latLng,
        consumeTapEvents: true,
        icon: BitmapDescriptor.fromBytes(markerIcon),
        onDragEnd: (newLoc) {
          updateNewMarkerLocation(id, newLoc, dragEnd: true);
        },
        onDrag: (newLoc) {
          updateNewMarkerLocation(id, newLoc);
        },
      ),
    );
  }

  generatePolylines(LatLng latlon) async {
    final Uint8List markerIcon =
        await HelperFunction.getBytesFromAsset(iconMarker, 90);
    polygonLatLongs.add(latlon);
    List<dynamic> list = [];

    addMarker(
        latLng: latlon,
        id: polygonLatLongs.length.toString(),
        markerIcon: markerIcon);

    setState(() {
      if (markers!.length > 2) {
        //create temp list to send lat longs so that first latLong is add to end
        List<LatLng> listTempPolygonLatLongs = [];
        polygonLatLongs.forEach((element) {
          listTempPolygonLatLongs.add(element);
        });
        listTempPolygonLatLongs.add(polygonLatLongs.first);
        removePolygon();
        polygonLatLongs.forEach((element) {
          list.add([element.longitude, element.latitude]);
        });

        if (polygonLatLongs.length >= 3) {
          list.add([
            polygonLatLongs.first.longitude,
            polygonLatLongs.first.latitude
          ]);
        }
        createPolygon(listTempPolygonLatLongs);
        widget.createPolygon!(listTempPolygonLatLongs);
      }
    });
  }

  /// this method we are using for creating the polygon
  /// isAreaContains is true then we are showing orange color else we are showing normal color
  createPolygon(List<LatLng> list, {bool isFieldContain = false}) {
    polygons!.add(Polygon(
        polygonId: PolygonId("NewPolygon${polygons!.length}"),
        points: list,
        fillColor: isAreaContains
            ? Colors.orange.withOpacity(.4)
            : isFieldContain
                ? Colors.orange.withOpacity(.4)
                : Colors.black38,
        strokeWidth: 4,
        strokeColor: isAreaContains
            ? Colors.orange
            : isFieldContain
                ? Colors.orange
                : Colors.lightGreen));
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _kGooglePlex != null
          ? Stack(
              children: <Widget>[
                GoogleMap(
                  gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                    new Factory<OneSequenceGestureRecognizer>(
                      () => new EagerGestureRecognizer(),
                    ),
                  ].toSet(),
                  mapType: isMapTypeNormal ? MapType.normal : MapType.hybrid,
                  initialCameraPosition: _kGooglePlex!,
                  mapToolbarEnabled: false,
                  myLocationEnabled: widget.myLocationEnabled == null
                      ? true
                      : widget.myLocationEnabled!,
                  myLocationButtonEnabled:
                      widget.myLocationButtonEnabled == null
                          ? true
                          : widget.myLocationButtonEnabled!,
                  zoomControlsEnabled: widget.zoomControlsEnabled == null
                      ? true
                      : widget.zoomControlsEnabled!,
                  //minMaxZoomPreference: widget.minMaxZoomPreference!,
                  markers: markers!,
                  polygons: polygons!,
                  polylines: polylines!,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);

                    if (widget.onMapCreated != null) {
                      widget.onMapCreated!(controller);
                    }
                  },
                  onCameraMoveStarted: () {
                    if (widget.onCameraMoveStarted != null) {
                      widget.onCameraMoveStarted!();
                    }
                  },
                  onCameraMove: (position) async {
                    mapZoomValue = position.zoom;
                    if (widget.onCameraMove != null) {
                      setState(() {
                        _lastMapPosition = position.target;
                      });
                      widget.onCameraMove!(position);
                    } else {
                      return;
                    }
                  },
                  onCameraIdle: () => {
                    widget.onCameraIdle != null ? widget.onCameraIdle!() : null,
                    if (_lastMapPosition != null) _getLocation()
                  },
                  onTap: (position) {
                    if (widget.createPolygon != null) {
                      if (!isPolygonCreated) {
                        generatePolylines(position);
                      }
                    } else {
                      _lastMapPosition =
                          LatLng(position.latitude, position.longitude);
                      if (widget.onTap != null) {
                        widget.onTap!(position, currentAddress);
                        _getLocation();
                      }
                    }
                  },
                ),
                Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(left: 12),
                      child: Column(
                        children: [
                          if (widget.onFullScreenClick != null)
                            WidgetMapToogleFullScreen(
                              onClick: (value) =>
                                  widget.onFullScreenClick!(value),
                            ),
                          actionButton(
                              Icon(Icons.map_rounded, color: Colors.grey[700]),
                              () {
                            setState(() {
                              isMapTypeNormal = !isMapTypeNormal;
                            });
                          }),
                          if (widget.searchedAddress != null)
                            actionButton(
                                Icon(Icons.search, color: Colors.grey[700]),
                                () => _handlePressButton()),
                        ],
                      ),
                    )),
                if (widget.createPolygon != null)
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 0, left: 65),
                      child: SingleChildScrollView(
                        child: Row(
                          children: [
                            actionButton(
                                const Icon(Icons.clear, color: Colors.red), () {
                              clearPolygon();
                            }),
                            const SizedBox(width: 15),
                            actionButton(
                                const Icon(Icons.undo, color: Colors.red), () {
                              if (polygonLatLongs.isNotEmpty) {
                                polygonLatLongs.removeLast();

                                markers!.removeWhere((element) =>
                                    element.markerId.value ==
                                    (polygonLatLongs.length + 1).toString());

                                setState(() {
                                  isPolygonCreated = false;
                                });
                                List<LatLng> listTempPolygonLatLongs = [];
                                polygonLatLongs.forEach((element) {
                                  listTempPolygonLatLongs.add(element);
                                });
                                if (polygonLatLongs.isNotEmpty) {
                                  listTempPolygonLatLongs
                                      .add(polygonLatLongs.first);
                                }
                                if (polygonLatLongs.isNotEmpty) {
                                  polygons!.removeWhere((element) =>
                                      element.polygonId ==
                                      PolygonId(
                                          "NewPolygon${polygons!.length - 1}"));
                                  polygons!.add(Polygon(
                                      polygonId: PolygonId(
                                          "NewPolygon${polygons!.length}"),
                                      points: listTempPolygonLatLongs,
                                      fillColor: Colors.black38,
                                      strokeWidth: 4,
                                      strokeColor: Colors.lightGreen));
                                }

                                widget.createPolygon!(listTempPolygonLatLongs);
                              }
                            }),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            )
          : Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 16),
                  height: 30,
                  width: 30,
                  child: const CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                ),
                const Text('Loading Map.')
              ],
            )),
    );
  }

  clearPolygon() {
    setState(() {
      //added below so to remove all markers which only form polygon
      polygonLatLongs.forEachIndexed((index, element) {
        markers!.removeWhere((markerData) =>
            markerData.markerId.value == (index + 1).toString());
      });
      isAreaContains = false;
      polylineLatLongs.clear();
      isPolygonCreated = false;
      removePolygon();
      polygonLatLongs.clear();
      widget.createPolygon!(polygonLatLongs);
    });
  }

  /// this method we are using to remove the last plot
  removePolygon() {
    polygons!.removeWhere((element) =>
        element.polygonId == PolygonId("NewPolygon${polygons!.length - 1}"));
  }

  Widget actionButton(Icon icon, Function onTap) {
    return Container(
        height: 38,
        width: 38,
        margin: const EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          color: Colors.white70,
        ),
        child: IconButton(icon: icon, onPressed: () => onTap()));
  }

  _getLocation() async {
    try {
      HelperFunction.getPlaceAddress(
              _lastMapPosition!.latitude, _lastMapPosition!.longitude)
          .then((value) {
        if (value == null) {
          HelperFunction.showFlushBarError(
              context, "Can't fetch your address please try again");
        } else {
          currentAddress = ModelAddress(
              locality: value.locality,
              address: value.address,
              pincode: value.pincode,
              district: value.district,
              taluka: value.taluka,
              state: value.state,
              village: value.village,
              country: value.country,
              latitude: value.latitude,
              longitude: value.longitude);
        }
        if (widget.searchedAddress != null) {
          widget.searchedAddress!(currentAddress);
        }
      });
    } catch (e) {
      return null;
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    HelperFunction.showFlushBarError(context, response.errorMessage);
  }

  Future<void> _handlePressButton() async {
    Prediction? p = await PlacesAutocomplete.show(
      context: context,
      apiKey: googleMapKey,
      onError: onError,
      types: [],
      strictbounds: false,
      mode: Mode.overlay,
      language: "En",
      components: [Component(Component.country, "In")],
    );

    displayPrediction(p, homeScaffoldKey.currentState, _controller)
        .then((value) async {
      final lat = value!.result.geometry!.location.lat;
      final lng = value.result.geometry!.location.lng;
      GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          bearing: 192.8334901395799,
          target: LatLng(lat, lng),
          zoom: mapZoomValue)));

      _lastMapPosition = LatLng(lat, lng);

      moveToNewPosition(lat, lng);
    });
  }

  Future<void> moveToNewPosition(lat, lon) async {
    CameraPosition newPosition = CameraPosition(
      target: LatLng(lat, lon),
      zoom: mapZoomValue,
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
        CameraUpdate.newLatLngZoom(newPosition.target, mapZoomValue));
    _onCameraMove(newPosition);
  }

  _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
    setState(() {
      mapZoomValue = position.zoom;
    });
  }

  // ignore: missing_return
  Future<PlacesDetailsResponse?> displayPrediction(
      Prediction? p,
      ScaffoldState? scaffold,
      Completer<GoogleMapController> _controller) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await GoogleMapsPlaces(apiKey: googleMapKey)
              .getDetailsByPlaceId(p.placeId!);
      return detail;
    }
  }

  /// this query we are using for validate the selected area
  /// if it contains any another plot area then we show error that user cannot select already selected plotI
}

class WidgetMapToogleFullScreen extends StatefulWidget {
  Function(bool) onClick;
  WidgetMapToogleFullScreen({super.key, required this.onClick});
  @override
  _WidgetMapToogleFullScreenState createState() =>
      _WidgetMapToogleFullScreenState();
}

class _WidgetMapToogleFullScreenState extends State<WidgetMapToogleFullScreen> {
  bool value = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 38,
        width: 38,
        margin: const EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          color: Colors.white70,
        ),
        child: IconButton(
          icon: Icon(!value ? Icons.fullscreen : Icons.fullscreen_exit,
              color: Colors.grey[700]),
          onPressed: () {
            setState(() {
              value = value ? false : true;
            });
            widget.onClick(value);
          },
        ));
  }
}
