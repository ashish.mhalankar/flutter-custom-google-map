class ModelAddress {
  String? id;
  String? address;
  String? district;
  String? taluka;
  String? locality;
  String? village;
  String? pincode;
  String? state;
  String? country;

  double? latitude;
  double? longitude;
  ModelAddress(
      {this.id,
      this.village,
      this.pincode,
      this.address,
      this.district,
      this.taluka,
      this.state,
      this.country,
      this.latitude,
      this.longitude,
      this.locality});
}
